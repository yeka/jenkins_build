Jenkins Build
=============

An alternative PHP way to build jenkins job.

Problem
-------
Bitbucket doesn't have a Post Hook option that trigerred by push to only specific branch (eg: master).
Jenkins doesn't have an option to build only specific branch when trigerred by Bicbucket Post Hook.

My Solution
-----------
As a PHP programmer, I use this script as a bridge between Jenkins and Bitbucket.
So when Bitbucket send a Post Hook, this script filter the branch and then send the request to Bitbucket if the branch match.

Bitbucket --> PHP Script --> Jenkins

How to use
----------
Given that ```http://localhost/jenkinsBuild/``` is reffering to my php script,

```
http://localhost/jenkinsBuild/?job=jenkins%20job%20name&token=mytoken
```
This will build only master branch for "jenkins job name" using "mytoken" as a token.

If you want to build other branch, add "branch" parameter.
```
http://localhost/jenkinsBuild/?job=jenkins%20job%20name&token=mytoken&branch=staging
```
This will build only staging branch for "jenkins job name" using "mytoken" as a token.

*Those link should be put in Bitbucket POST Hook

Requirements
------------

For Jenkins side:

[Build Authorization Token Root Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Build+Token+Root+Plugin)
so that Jenkins can build a job without login.


[Bitbucket Plugin](https://wiki.jenkins-ci.org/display/JENKINS/BitBucket+Plugin)
to works with bitbucket

My nginx configuration:
```
server {
    listen 80;
    server_name localhost;

    location / {
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header Host $host;
        proxy_pass http://127.0.0.1:8080;
    }

    location /jenkinsBuild {
        root /var/www/jenkinsBuild/;
        index index.php;
        fastcgi_index index.php;
        fastcgi_pass 127.0.0.1:9001;
        include fastcgi.conf;

        fastcgi_buffer_size 128k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;
    }
}
```