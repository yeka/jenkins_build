<?php
if (!empty($_GET['file']) && $_GET['file'] == 'list') {
	showList();
	die;
}
if (!empty($_GET['file'])) {
	showFile($_GET['file']);
	die;
}

$date = getMyDate();
$content = getMyContent();
$file = __DIR__.'/logs/'.$date.'.json';
$c = file_put_contents($file, $content);
//var_dump($c);

$request = json_decode($content);
$branchToBuild = 'master';

checkNormalPush($request, $branchToBuild);
checkPullRequestMerged($request, $branchToBuild);
// echo "[OK]";

function showList()
{
	$a = scandir(__DIR__.'/logs/');
	rsort($a);
	foreach ($a as $file) {
		if (in_array($file, array('.', '..'))) continue;
		echo sprintf('<a href="?file=%1$s">%1$s</a><br/>', $file);
	}
}

function showFile($file)
{
	header('Content-Type: application/json');
	echo file_get_contents(__DIR__.'/logs/'.$file);
}

function getMyDate()
{
	return date('Y-m-d-H-i-s');
}

function getMyContent()
{
	$data = [
		'server' => $_SERVER,
		'get' => $_GET,
		'post' => $_POST,
		'cookie' => $_COOKIE,
		'files' => $_FILES,
		'json' => file_get_contents('php://input'),
	];
	if (!empty($data['post']['payload'])) {
			$data['post']['payload'] = json_decode($data['post']['payload']);
	}
	if (!empty($data['json'])) {
			$data['json'] = json_decode($data['json']);
	}
	return json_encode($data);
}

function checkNormalPush($d, $branchToBuild)
{
	if (empty($d->get->token) || empty($d->get->job) || empty($d->post->payload->commits)) {
		return false;
	}

	$branchToBuild = empty($d->get->branch) ? $branchToBuild : $d->get->branch;

	$params = array(
		'job' => $d->get->job,
		'token' => $d->get->token,
	);

	foreach ($d->post->payload->commits as $commit) {
		if (!empty($commit->branch) && $commit->branch == $branchToBuild) {
			$params['branch'] = $commit->branch;
			build($params);
			break;
		}
	}
}

function checkPullRequestMerged($d, $branchToBuild)
{
	if (empty($d->get->token) || empty($d->get->job) || empty($d->json->pullrequest_merged->destination->branch->name)) {
		return false;
	}

	$branchToBuild = empty($d->get->branch) ? $branchToBuild : $d->get->branch;

	$params = array(
		'job' => $d->get->job,
		'token' => $d->get->token,
		'branch' => $d->json->pullrequest_merged->destination->branch->name,
	);

	if ($params['branch'] == $branchToBuild) {
		build($params);
	}
}

function build($params)
{
	$jenkinsHost = "http://127.0.0.1:8080";
	$build_url = "/buildByToken/buildWithParameters?job=%s&token=%s&branch=%s";
	$url = $jenkinsHost.sprintf(
		$build_url, 
		urlencode($params['job']),
		urlencode($params['token']),
		urlencode($params['branch'])
	);
	callUrl($url);
}

function callUrl($url)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	echo curl_exec($ch);
	curl_close($ch);
}