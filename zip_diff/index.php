<?php

zipDiff('a.zip', 'b.zip', 'c.zip');

function zipDiff($old_file, $updated_file, $diff_file)
{
    $a = new ZipArchive();
    $a->open($old_file);

    $b = new ZipArchive();
    $b->open($updated_file);

    $c = new ZipArchive();
    $c->open($diff_file, ZipArchive::OVERWRITE);

    for ($i = 0; $i < $b->numFiles; $i++) {
        $b_info = $b->statIndex($i);
        $a_info = $a->statName($b_info['name']);

        if ($a_info == false // File is new
            || $a_info['size'] != $b_info['size'] // File is changed
            || $a_info['crc'] != $b_info['crc'] // File is changed
            ) {
            $c->addFromString($b_info['name'], $b->getFromIndex($i));
        }
    }

    $removed_list = array();
    for ($i = 0; $i < $a->numFiles; $i++) {
        $a_info = $a->statIndex($i);
        $b_info = $b->statName($a_info['name']);
        if ($b_info == false) {
            $removed_list[] = $a_info['name'];
        }
    }
    $c->setArchiveComment(json_encode(array('removed_files' => $removed_list)));

    $a->close();
    $b->close();
    $c->close();
}
